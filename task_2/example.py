import time
from timeit import timeit

from lru_cache import lru_cache


def very_time_consuming_func(a, b):
    time.sleep(1)
    return a + b


ARGS = [
    (1, 2),  # executed
    (1, 2),  # cache
    (3, 4),  # executed
    (13, 20),  # executed
    (20, 13),  # executed
    (1, 2),  # cache
    (1, 2),  # cache
    (13, 20),  # cache
    (4, 5),  # executed
    (6, 7),  # executed
    (4, 5),  # cache
]


def run_func(func, add_cache=False):
    if add_cache:
        func = lru_cache(4)(func)
    for a in ARGS:
        func(*a)


if __name__ == '__main__':
    print 'Running test - expected results - cached func' \
          'should be around 5 sec faster each run, about 50 in total'
    print 'Without cache:'
    print timeit('run_func(very_time_consuming_func)',
                 'from __main__ import very_time_consuming_func, run_func',
                 number=10)
    print 'With cache: '
    print timeit(
        'run_func(very_time_consuming_func, True)',
        'from __main__ import very_time_consuming_func, run_func',
        number=10,
    )
