from unittest import TestCase


class TestCache(TestCase):
    def test_decorator_does_not_change_value_args(self):
        from task_2.lru_cache import lru_cache

        def func_1(int_1, int_2, str_1):
            """sample function"""
            ords = [ord(l) for l in str_1]
            return sum(ords) + int_1 * int_2

        cached_func_1 = lru_cache(10)(func_1)

        for i in range(11):  # bigger than cache
            self.assertEqual(
                func_1(1 + i, 20 - 1, 'test_2'),
                cached_func_1(1 + i, 20 - 1, 'test_2')
            )

    def test_decorator_does_not_change_value_kwargs(self):
        from task_2.lru_cache import lru_cache

        def func_1(int_1, int_2, str_1):
            """sample function"""
            ords = [ord(l) for l in str_1]
            return sum(ords) + int_1 * int_2

        cached_func_1 = lru_cache(10)(func_1)

        for i in range(11):  # bigger than cache
            self.assertEqual(
                func_1(int_1=1 + i, int_2=20 - 1, str_1='test_2'),
                cached_func_1(int_1=1 + i, int_2=20 - 1, str_1='test_2')
            )

    def test_cache_create_key_equal(self):
        from task_2.lru_cache import LRUCache

        lru_object = LRUCache(4, lambda _: True)

        self.assertEqual(lru_object.create_key(1, 2, 3, a=4, test='test'),
                         lru_object.create_key(1, 2, 3, a=4, test='test'))

        self.assertEqual(lru_object.create_key(1, 2, 3, a=4, test='test'),
                         lru_object.create_key(1, 2, 3, a=4, test='test'))

        self.assertEqual(lru_object.create_key(1, 2, 3, a=4, test='test'),
                         lru_object.create_key(1, 2.0, 3, a=4, test='test'))

        self.assertEqual(
            lru_object.create_key(1, 2.0, 3, a=4, test='test'),
            lru_object.create_key(1, 2.0, 3, a=4.0, test='test'),
        )

    def test_cache_create_key_not_equal(self):
        from task_2.lru_cache import LRUCache

        lru_object = LRUCache(4, lambda _: True)

        self.assertNotEqual(
            lru_object.create_key(1, 2, 3, a=4, test='test'),
            lru_object.create_key(1, 2, a=4, test='test'))

        self.assertNotEqual(lru_object.create_key(1, 2, 3, a=4),
                            lru_object.create_key(1, 8, 3, a=4, cd='cdn'))

        self.assertNotEqual(
            lru_object.create_key(1.0, 2, 3, a=4, test='test'),
            lru_object.create_key(1.00000000001, 2.0, 3, a=4, test='test')
        )

    def test_cache_size(self):
        from task_2.lru_cache import lru_cache

        @lru_cache(max_size=10)
        def func(a, b):
            return a ** 2 + 2 * a * b * b ** 2

        for i in range(10):
            func(i + 10, 20 - i)
            self.assertFalse(func.cache_stats.last_hit)
            self.assertEqual(func.cache_stats.cache_size, i + 1)

    def test_hit_and_miss_count(self):
        from task_2.lru_cache import lru_cache

        @lru_cache(max_size=20)
        def func(a, b):
            return a ** 2 + 2 * a * b * b ** 2

        for i in range(5):
            func(i % 5, 10)
            self.assertFalse(func.cache_stats.last_hit)
            self.assertEqual(func.cache_stats.cache_size, i + 1)
        self.assertEqual(func.cache_stats.miss_count, 5)
        self.assertEqual(func.cache_stats.hit_count, 0)

        for i in range(5):
            func(i % 5, 10)
            self.assertTrue(func.cache_stats.last_hit)
            self.assertEqual(func.cache_stats.cache_size, 5)
        self.assertEqual(func.cache_stats.miss_count, 5)
        self.assertEqual(func.cache_stats.hit_count, 5)

    def test_wiki_cache(self):
        from task_2.lru_cache import lru_cache

        @lru_cache(max_size=4, debug=True)
        def func(a):
            return sum([ord(s) for s in a])

        func('A')
        self.assertEqual(set(func.cache_object.cache.keys()),
                         {('A',)})

        func('B')
        self.assertEqual(set(func.cache_object.cache.keys()),
                         {('A',), ('B',)})

        func('C')
        self.assertEqual(set(func.cache_object.cache.keys()),
                         {('A',), ('B',), ('C',)})

        func('D')
        self.assertEqual(set(func.cache_object.cache.keys()),
                         {('A',), ('B',), ('C',), ('D',)})

        func('E')
        self.assertEqual(set(func.cache_object.cache.keys()),
                         {('E',), ('B',), ('C',), ('D',)})

        func('D')
        self.assertEqual(set(func.cache_object.cache.keys()),
                         {('E',), ('B',), ('C',), ('D',)})

        func('F')
        self.assertEqual(set(func.cache_object.cache.keys()),
                         {('E',), ('F',), ('C',), ('D',)})

    def test_last_key_hit(self):
        from task_2.lru_cache import lru_cache

        @lru_cache(max_size=4, debug=True)
        def func(a):
            return sum([ord(s) for s in a])

        for _ in range(5):
            func('A')

        self.assertEqual(len(func.cache_object.calls), 1)

    def test_last_key_miss(self):
        from task_2.lru_cache import lru_cache

        @lru_cache(max_size=4, debug=True)
        def func(a):
            return sum([ord(s) for s in a])

        for _ in range(5):
            func('A')
            func('B')

        self.assertEqual(len(func.cache_object.calls), 10)

    def calls_deque_trimmed(self):
        from task_2.lru_cache import lru_cache

        @lru_cache(max_size=4, debug=True)
        def func(a):
            return sum([ord(s) for s in a])

        func('B')
        for _ in range(5):
            func('A')
        self.assertEqual(len(func.cache_object.calls), 6)
        func('C')
        self.assertEqual(len(func.cache_object.calls), 7)
        func('D')  # cache full
        self.assertEqual(len(func.cache_object.calls), 8)
        func('E')  # B dropped
        self.assertEqual(len(func.cache_object.calls), 8)
        func('C')  # trim As
        self.assertEqual(len(func.cache_object.calls), 4)
