from collections import deque
from functools import wraps


class CacheStats(object):
    def __init__(self):
        self.last_hit = 0
        self.hit_count = 0
        self.miss_count = 0
        self.cache_size = 0


class LRUCache(object):
    """
    LRU cache which can be used standalone, or with `lru_cache` decorator
    - stores all calls, trimming the list if possible
    - stores the values based on the args and kwargs of the call; known issue -
      currently swapping args and kwargs will not be recognized, so for:
      def func(a, b):
          pass
      calls func(1, 2), func(1, b=2), func(a=1, b=2) - each will be stored
      separately.
    """
    def __init__(self, max_size, func):
        self.cache = {}
        self.func = func

        self.max_size = max_size
        self.calls = deque()
        self.counter = 0

        # statistics
        self.stats = CacheStats()
        self.last_key_hit = None

    def update_stats_hit(self):
        self.stats.hit_count += 1
        self.stats.last_hit = True

    def update_stats_miss(self):
        self.stats.miss_count += 1
        self.stats.last_hit = False

    def trim_calls(self):
        while self.calls:
            key, age = self.calls[0]
            if self.cache[key]['age'] > age:
                self.calls.popleft()
            else:
                return

    def store_value(self, key, value):
        if len(self.cache) == self.max_size:
            # got to remove
            key_to_drop = None
            while not key_to_drop:
                key_candidate, age = self.calls.popleft()
                if self.cache[key_candidate]['age'] == age:
                    # wasn't called since being stored
                    key_to_drop = key_candidate
            del self.cache[key_to_drop]

        self.counter += 1
        self.cache[key] = {
            'value': value,
            'age': self.counter,
        }
        self.calls.append((key, self.counter))
        self.last_key_hit = key
        self.stats.cache_size = len(self.cache)

    def store_value_read(self, key):
        if key != self.last_key_hit:
            self.counter += 1
            self.cache[key]['age'] = self.counter
            self.calls.append((key, self.counter))
            self.last_key_hit = key

    def eval_func(self, *args, **kwargs):
        key = self.create_key(*args, **kwargs)

        try:
            value = self.cache[key]['value']

            self.update_stats_hit()
            self.store_value_read(key)
        except KeyError:
            value = self.func(*args, **kwargs)

            self.update_stats_miss()
            self.store_value(key, value)

        return value

    def create_key(self, *args, **kwargs):
        if args and kwargs:
            return args, tuple(kwargs.items())
        elif not kwargs:
            return args
        return tuple(kwargs.items())


def lru_cache(max_size=100, debug=False):
    """"""
    def wrapper(func):
        cache_object = LRUCache(max_size, func)

        @wraps(func)
        def cache_decorator(*args, **kwargs):
            return cache_object.eval_func(*args, **kwargs)

        cache_decorator.cache_stats = cache_object.stats

        if debug:
            cache_decorator.cache_object = cache_object

        return cache_decorator

    return wrapper
