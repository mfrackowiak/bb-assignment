from datetime import datetime
from unittest import TestCase


class TestDateFind(TestCase):
    def test_day_before_day_1(self):
        from task_1.lottery import next_draw

        next_lottery = next_draw(datetime(2018, 7, 2, 12, 0, 0))

        self.assertEqual(next_lottery,
                         datetime(2018, 7, 3, 20, 0, 0))

    def test_day_before_day_2(self):
        from task_1.lottery import next_draw

        next_lottery = next_draw(datetime(2018, 7, 6, 12, 0, 0))

        self.assertEqual(next_lottery,
                         datetime(2018, 7, 7, 20, 0, 0))

    def test_time_before_day_1(self):
        from task_1.lottery import next_draw

        next_lottery = next_draw(datetime(2018, 7, 3, 15, 0, 0))

        self.assertEqual(next_lottery,
                         datetime(2018, 7, 3, 20, 0, 0))

    def test_time_before_day_2(self):
        from task_1.lottery import next_draw

        next_lottery = next_draw(datetime(2018, 7, 7, 19, 59, 59))

        self.assertEqual(next_lottery,
                         datetime(2018, 7, 7, 20, 0, 0))

    def test_time_after_day_1(self):
        from task_1.lottery import next_draw

        next_lottery = next_draw(datetime(2018, 7, 3, 20, 0, 1))

        self.assertEqual(next_lottery,
                         datetime(2018, 7, 7, 20, 0, 0))

    def test_time_after_day_2(self):
        from task_1.lottery import next_draw

        next_lottery = next_draw(datetime(2018, 7, 7, 22, 0, 12))

        self.assertEqual(next_lottery,
                         datetime(2018, 7, 10, 20, 0, 0))

    def test_new_year(self):
        from task_1.lottery import next_draw

        next_lottery = next_draw(datetime(2017, 12, 31, 23, 59, 59))

        self.assertEqual(next_lottery,
                         datetime(2018, 1, 2, 20, 0, 0))

    def test_invalid_input(self):
        from task_1.lottery import next_draw

        with self.assertRaises(ValueError):
            next_draw('2018-05-16')
