from datetime import datetime, time, timedelta

LOTTERY_DAY_1 = 1
LOTTERY_DAY_2 = 5
LOTTERY_HOUR = 20


def next_draw(after_datetime=None):
    if after_datetime and not isinstance(after_datetime, datetime):
        raise ValueError('Valid datetime is expected')

    after_datetime = after_datetime or datetime.now()
    lottery_hour = time(LOTTERY_HOUR, 0, 0)

    after_weekday = after_datetime.weekday()
    if after_weekday in {LOTTERY_DAY_1, LOTTERY_DAY_2} and \
            after_datetime.time() < lottery_hour:
        lottery_date = after_datetime.date()
    elif after_weekday >= LOTTERY_DAY_2 or after_weekday < LOTTERY_DAY_1:
        day_difference = timedelta(days=(LOTTERY_DAY_1 - after_weekday) % 7)
        lottery_date = after_datetime.date() + day_difference
    else:
        day_difference = timedelta(days=(LOTTERY_DAY_2 - after_weekday) % 7)
        lottery_date = after_datetime.date() + day_difference

    return datetime.combine(lottery_date, lottery_hour)
