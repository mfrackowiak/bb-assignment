from collections import defaultdict
from unittest import TestCase


class AnagramTest(TestCase):
    def test_empty_anagram(self):
        from task_3.anagram import anagram

        results = anagram('', [])

        self.assertEqual(results, [])

    def test_anagrams_1(self):
        from task_3.anagram import anagram

        results = anagram('test',
                          ['test', 'tste', 'tsst', 'tes', 'sett', 'set'])

        self.assertEqual(results, ['test', 'tste', 'sett'])

    def test_word_letters_empty(self):
        from task_3.anagram import word_letters

        results = word_letters('')

        self.assertEqual(results, defaultdict(lambda: 0))

    def test_word_letters_string(self):
        from task_3.anagram import word_letters

        results = word_letters('test')

        self.assertEqual(dict(results),
                         {'t': 2, 'e': 1, 's': 1})

        results = word_letters('acaxaxaeaxcxf')

        self.assertEqual(dict(results),
                         {'a': 5, 'x': 4, 'c': 2, 'e': 1, 'f': 1})

    def test_is_anagram(self):
        from task_3.anagram import is_anagram

        self.assertTrue(is_anagram({'a': 2, 'b': 3, 'd': 4}, 'aabbbdddd'))
        self.assertFalse(is_anagram({'t': 2, 'e': 1, 's': 1}, 'teest'))
        self.assertFalse(is_anagram({'t': 2, 'e': 1, 's': 1}, 'ufduu'))
        self.assertFalse(is_anagram({'a': 2, 'b': 3, 'd': 4, 'f': 1},
                                    'aabbbdddd'))

    def test_invalid_word(self):
        from task_3.anagram import anagram

        with self.assertRaises(ValueError):
            anagram(123, ['test', 'test2'])

    def test_invalid_list(self):
        from task_3.anagram import anagram

        with self.assertRaises(ValueError):
            anagram('test', 123)

        with self.assertRaises(ValueError):
            anagram('test', [1, 2, 5])
