from collections import defaultdict
from functools import partial


def word_letters(word):
    letters_count = defaultdict(lambda: 0)

    for letter in word:
        letters_count[letter] += 1

    return letters_count


def is_anagram(base_anagram_letters, anagram_candidate_word):
    anagram_candidate_letters = word_letters(anagram_candidate_word)
    for letter, count in anagram_candidate_letters.items():
        if base_anagram_letters.get(letter, 0) != count:
            return False
    return base_anagram_letters.keys() == anagram_candidate_letters.keys()


def anagram(word, candidates):
    if not isinstance(word, str):
        raise ValueError('Expected valid str as the word')

    if not isinstance(candidates, (list, tuple)) or \
        not all(map(lambda i: isinstance(i, str), candidates)):
        raise ValueError('Expected valid list of str as the candidate words')

    base_word_letters = word_letters(word)

    return filter(
        partial(is_anagram, base_word_letters),
        candidates,
    )
